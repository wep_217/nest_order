import { IsNotEmpty } from 'class-validator';

class CreatedOrderItemDto {
  @IsNotEmpty()
  productId: number;

  @IsNotEmpty()
  amount: number;
}
export class CreateOrderDto {
  @IsNotEmpty()
  customerID: number;

  @IsNotEmpty()
  orderItems: CreatedOrderItemDto[];
}
